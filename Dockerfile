# The first instruction is what image we want to base our container on
# We Use an official Python runtime as a parent image
FROM alpine

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache \
  sudo \
  bash \
  build-base \
  python-dev \
  nginx \
  supervisor \
  sqlite \
  python \
  uwsgi-python \
  py-pip \
  mariadb-dev \
  memcached \
  linux-headers \
  jpeg-dev \
  zlib-dev

RUN mkdir /easyreservations

WORKDIR /easyreservations

ADD . /easyreservations/


# Copy the entrypoint
COPY ./docker/conf/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# install python dependencies here
RUN pip install --no-cache-dir --upgrade pip && \
  apk --update add --virtual build-dependencies git g++  && \
  pip install --no-cache-dir -r requirements.txt && \
  apk del build-dependencies


RUN  python manage.py collectstatic --noinput

EXPOSE 8000


