angular
    .module('easyreservation')
    .service('reservationsService', ['$http', '$q', reservationsService]);

function reservationsService($http, $q) {

    this.getReservations= function() {
      var deferred = $q.defer();
      $http({
          method: 'GET',
          url: '/api/reservations/',
      })
      .then(function(response){
          deferred.resolve(response.data);
      })
      .catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
    }

    this.getReservation= function(id) {
      var deferred = $q.defer();
      url = '/api/reservations/'.concat(id).concat('/details')
      $http({
          method: 'GET',
          url: url,
      })
      .then(function(response){
          deferred.resolve(response.data);
      })
      .catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
    }

    this.deleteReservation= function(id) {
      var deferred = $q.defer();
      url = '/api/reservations/'.concat(id).concat('/delete')
      $http({
          method: 'DELETE',
          url: url,
      })
      .then(function(response){
          deferred.resolve(response.data);
      })
      .catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
    }

    this.updateReservation= function(id, data) {
      var deferred = $q.defer();
      url = '/api/reservations/'.concat(id).concat('/edit/')
      $http({
          method: 'put',
          url: url,
          data: JSON.stringify(data),
          headers: {'Content-Type': 'application/json'}
      })
      .then(function(response){
          deferred.resolve(response.data);
      })
      .catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
    }
}

