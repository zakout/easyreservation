angular
    .module('easyreservation')
    .service('resourceService', ['$http', '$q', resourceService]);

function resourceService($http, $q) {

    this.getResources = function(params) {
      var deferred = $q.defer();
      $http({
          method: 'GET',
          url: '/api/resources/',
          params: params
      })
      .then(function(response){
          deferred.resolve(response.data);
      })
      .catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
    }

    this.deleteResources = function(id) {
      var url = '/api/resources/'.concat(id).concat('/delete/')
      var deferred = $q.defer();
      $http({
          method: 'DELETE',
          url: url,
      })
      .then(function(response){
          deferred.resolve(response.data);
      })
      .catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
    }
}

