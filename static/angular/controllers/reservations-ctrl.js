angular
    .module('easyreservation')
    .controller('reservationsController', ['$scope', '$window', '$compile', '$timeout', 'uiCalendarConfig', 'reservationsService', reservationsController]);

function reservationsController($scope, $window, $compile, $timeout, uiCalendarConfig, reservationsService) {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.changeTo = 'Hungarian';
    $scope.currentView = 'month';
    $scope.events = new Array()

    reservationsService.getReservations().then(function(response){
        $scope.reservations = response
        /* event source that contains custom events on the scope */
        for(var i=0;i<$scope.reservations.length ;i++) {
            var dates = $scope.reservations[i].reservation_time.split(' - ')
            $scope.events.push({
                title: $scope.reservations[i].title,
                start: new Date(dates[0]),
                end: new Date(dates[1]),
                id: $scope.reservations[i].id
            })
        }
    })


    /* event source that calls a function on every view switch */

    $scope.ev = {};


    /* alert on eventClick */
    $scope.alertOnEventClick = function (date, jsEvent, view) {
        $window.location.href = '/reservations/'.concat(date.id).concat('/detail')
    };



    /* Change View */
    $scope.changeView = function (view, calendar) {
        $scope.currentView = view;
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
    };
    /* Change View */
    $scope.renderCalender = function (calendar) {
        $timeout(function () {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        });
    };
    /* Render Tooltip */
    $scope.eventRender = function (event, element, view) {};
    /* config object */
    $scope.uiConfig = {
        calendar: {
            stick: true,
            height: 450,
            editable: true,

            header: {
                left: 'title',
                right: 'today prev,next'
            },
            eventClick: $scope.alertOnEventClick,
            businessHours: {
                start: '08:00', // a start time (10am in this example)
                end: '20:00', // an end time (6pm in this example)

                dow: [1, 2, 3, 4]
                // days of week. an array of zero-based day of week integers (0=Sunday)
                // (Monday-Thursday in this example)
            }
        }
    };

    /* event sources array*/
    $scope.eventSources = [$scope.events];
};