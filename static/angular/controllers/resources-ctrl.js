angular
    .module('easyreservation')
    .controller('resourceController', ['$scope', '$window', '$timeout', 'resourceService', resourceController]);

function resourceController($scope, $window, $timeout, resourceService) {
    $scope.resources = []
    resourceService.getResources().then(function(response){
        $scope.resources = response.results
        console.log(response)
    })

    $scope.deleteResource = function(id) {
        resourceService.deleteResources(id).then(function(response){
            $scope.resources = $scope.resources.filter(function(obj) {
                return obj.id !== id
            })
        })
    }

    $scope.filterResource = function() {
        resourceService.getResources({'q': $scope.filter}).then(function(response){
            $scope.resources = response.results
            console.log(response)
        })
    }
};