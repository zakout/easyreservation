angular
    .module('easyreservation')
    .controller('reservationController', ['$scope', '$window', 'reservationsService', 'resourceService', reservationController]);

function reservationController($scope, $window, reservationsService, resourceService) {
    $scope.$watch('id', function(newValue, oldValue) {
        reservationsService.getReservation($scope.id).then(function(response) {
            $scope.reservation = response
            $scope.title = $scope.reservation.title
            $scope.resource = $scope.reservation.resource
            $scope.reservation_time = $scope.reservation.reservation_time

            var dates = $scope.reservation.reservation_time.split(' - ')
            if (new Date(dates[0]) < new Date()) {
                $scope.disableUpdate = true
            }
            if (new Date(dates[1]) < new Date()) {
                $scope.disableDelete = true
            }

            resourceService.getResources().then(function(response) {
                $scope.selected_resource_id = response.results.filter(function(obj) {
                    return obj.slug === $scope.reservation.resource
                })[0].id
                $scope.selected_resource_title = response.results.filter(function(obj) {
                    return obj.slug === $scope.reservation.resource
                })[0].title
                $scope.resources = response.results.filter(function(obj) {
                    return obj.id !== $scope.reservation.id
                })
            })
        })

    })

    $scope.deleteReservation = function(id) {
        reservationsService.deleteReservation(id).then(function() {
            $window.location.href = '/reservations/'
        })
    }

    $scope.updateReservation = function(id) {
        data = {
            'title': $scope.title,
            'resource': angular.element('#id_resource').val(),
            'reservation_time': $scope.reservation_time,
        }
        console.log(data)
        reservationsService.updateReservation(id, data).then(function() {
             $window.location.href = '/reservations/'
        })
    }

};