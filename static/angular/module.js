var app = angular.module('easyreservation', ['ui.calendar', 'ui.bootstrap'])

app.config(function($interpolateProvider, $httpProvider, $locationProvider,) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

app.controller('baseController', function($scope, $http, $window) {
    $scope.changeSettings = function() {
        var params = {}
        params["code"] = angular.element('#id_code').val()

        params["time_zone"] =  angular.element('#id_timeZone').val(),

        $http({
           method: 'GET',
           url: '/api/users/switch_settings/',
           params: params
        }).then(function succssCallback(response) {
           $window.location.reload();
        }, function errorCallback(response) {

        });
    }
});
