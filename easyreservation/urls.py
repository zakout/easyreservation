"""easyreservation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

from django.contrib import admin
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$',
        auth_views.logout,
        {'next_page': '/login/'},
        name='logout'
        ),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'^admin/', admin.site.urls),
    url('api/users/',
        include('accounts.api.urls', namespace='users-api')
        ),
    url('api/resources/',
        include('resources.api.urls', namespace='resources-api')
        ),
    url('api/reservations/',
        include('reservations.api.urls', namespace='reservations-api')
        ),

    url('accounts/', include('accounts.urls', namespace='accounts')),

    url('resources/', include('resources.urls', namespace='resources')),
    url('reservations/',
        include('reservations.urls', namespace='reservations')),
    url(r'', include('reservations.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT
                          )
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
