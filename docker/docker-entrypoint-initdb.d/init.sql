GRANT ALL ON *.* to root@localhost IDENTIFIED BY 'admin';
GRANT ALL ON *.* to root@'%' IDENTIFIED BY 'admin';

FLUSH PRIVILEGES;

DELETE IGNORE FROM auth_user WHERE username="admin";
DELETE IGNORE FROM auth_user WHERE username="demo";
DELETE IGNORE FROM auth_user WHERE username="mohamed";


INSERT INTO easyreservation.auth_user (id,password,last_login,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES
(1,'pbkdf2_sha256$36000$u6O7ppZ6ca2w$oVN7Y4IDgVr+aXDcbWtNT/pz6Qhyl+NgvcRIJYa0lyk=','2018-10-29 11:24:22.150',0,'zarrouk','','','zarrouk@eurecom.fr',1,1,'2018-10-14 00:15:29.000')
,(2,'pbkdf2_sha256$36000$wIPj9rvpTji0$dwgGB0r+SEC3I/do81GwMFTMBuBpt2NrDxW+wOVzEzk=','2018-10-29 11:31:15.059',1,'sabry','Sabry','Stita','sabry@live.fr',1,1,'2018-10-18 00:06:45.000')
,(3,'pbkdf2_sha256$36000$HQbgIRj9XyLt$Pd9fBXBKTET7KJr81yhl2Sbe1lM6HwO7J05/S0zuwec=',NULL,0,'bechir','','','',0,1,'2018-10-18 00:09:15.877')
,(4,'!pChHdb2jS6gIrcKsKvEAVkxCaWErsfV2gSO0BXvD','2018-10-22 06:34:24.374',0,'Mohamed','Mohamed','Zarrouk','',0,1,'2018-10-21 06:08:56.249')
,(5,'pbkdf2_sha256$36000$IyOhHFZbqdq9$A06ETVgZafIwbpkTyV+ykbqzHk2jirB5qebXDuqRzQY=',NULL,1,'admin','','','admin@admin.fr',1,1,'2018-10-28 19:14:15.450')
,(6,'pbkdf2_sha256$36000$eGj74WwCqNfV$KC0HnF4c8CuLhLEfKFTA973y3NNaJF86ql/fYrNVWCw=','2018-10-29 11:23:54.507',1,'tester','tester','tester','tester@live.fr',0,1,'2018-10-28 19:58:40.000')
,(7,'pbkdf2_sha256$36000$c050UicLDiOz$H914E5q5jVn5+wzP1pcjXrdW1ZC2J/qnpjcqBCzr/d0=',NULL,1,'demo','Sharing','CLOUD','demo@sharingcloud.com',0,1,'2018-10-29 12:48:17.000')
;

INSERT INTO easyreservation.resources (id,title,slug,location,max_person,state,`type`,`timestamp`,user_id) VALUES
(1,'Salle 1','salle-1','France',10,1,1,'2018-10-14 06:13:59.576',1)
,(4,'Salle 2','salle-2-e2hv','France',10,1,1,'2018-10-14 06:13:59.576',1)
,(5,'Salle 3','salle-3','France',10,1,1,'2018-10-14 06:13:59.576',1)
,(6,'Bateau 1','bateau-1','Paris',10,1,3,'2018-10-14 06:13:59.576',1)
,(7,'Salle 10','salle-10','London',11,1,1,'2018-10-14 06:13:59.576',1)
,(8,'Bateau 2','bateau-2','Tunis',10,1,3,'2018-10-14 07:11:25.588',1)
,(9,'Bateau 3','bateau-3','Londre',10,1,3,'2018-10-14 07:11:49.657',1)

﻿INSERT INTO easyreservation.resrource_types (id,`type`,image,height_field,width_field) VALUES
(1,'Salle','Salle.png',183,252)
,(2,'Restaurant','Restaurant.png',183,275)
,(3,'Bateau','Bateau.png',375,701)
;
