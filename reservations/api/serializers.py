# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
)

from reservations.models import Reservation


class ReservationSerializer(ModelSerializer):
    user = SerializerMethodField()
    resource = SerializerMethodField()

    class Meta:
        model = Reservation
        fields = [
            'id',
            'user',
            'resource',
            'title',
            'reservation_time',
        ]

    def get_user(self, obj):
        return str(obj.user.username)

    def get_resource(self, obj):
        return obj.resource.slug


class ReservationCreateUpdateSerializer(ModelSerializer):
    class Meta:
        model = Reservation
        fields = [
            'resource',
            'title',
            'reservation_time',
        ]