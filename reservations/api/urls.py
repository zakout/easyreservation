from django.conf.urls import url

from .views import (
    ReservationCreateAPIView,
    ReservationListAPIView,
    ReservationDetailAPIView,
    ReservationDestroyAPIView,
    ReservationUpdateAPIView
)

urlpatterns = [
    url(
        r'^$',
        ReservationListAPIView.as_view(),
        name='list'
    ),
    url(
        r'^create/$',
        ReservationCreateAPIView.as_view(),
        name='create'
    ),
    url(
        r'^(?P<pk>[\w-]+)/details/$',
        ReservationDetailAPIView.as_view(),
        name='details'
    ),
    url(
        r'^(?P<pk>[\w-]+)/edit/$',
        ReservationUpdateAPIView.as_view(),
        name='update'
    ),
    url(
        r'^(?P<pk>[\w-]+)/delete/$',
        ReservationDestroyAPIView  .as_view(),
        name='delete'
    ),
]
