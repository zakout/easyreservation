from datetime import datetime
import pytz

from django.utils.timezone import localtime
from django.utils import timezone

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    UpdateAPIView,
    RetrieveAPIView,
)

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly
)

from reservations.models import Reservation
from accounts.models import Profile

from .permissions import isOwnerOrAdmin

from .serializers import (
    ReservationSerializer,
    ReservationCreateUpdateSerializer,
)


class ReservationCreateAPIView(CreateAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationCreateUpdateSerializer
    permission_classes = [IsAuthenticated, isOwnerOrAdmin]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ReservationDetailAPIView(RetrieveAPIView):
    queryset = Reservation.objects.all()

    serializer_class = ReservationSerializer
    permission_classes = [IsAuthenticated, isOwnerOrAdmin]


    def get_object(self, *args, **kwargs):
        obj = super(ReservationDetailAPIView, self).get_object(*args, **kwargs)
        if self.request.session.get('django_timezone'):
            tzname = self.request.session.get('django_timezone')
        else:
            tzname = Profile.objects.filter(user=self.request.user).first().time_zone
            self.request.session['django_timezone'] = tzname
        if tzname:
            timezone.activate(pytz.timezone(tzname))
        else:
            timezone.deactivate()
        dates = obj.reservation_time.split(' - ')
        time_zone = obj.time_zone
        if time_zone:
            tz = pytz.timezone(time_zone)
        else:
            tz = pytz.timezone('UTC')
        local_tz = pytz.timezone(tzname)
        start = local_tz.localize(
            datetime.strptime(dates[0], '%m/%d/%Y %I:%M %p'))
        end = local_tz.localize(
            datetime.strptime(dates[1], '%m/%d/%Y %I:%M %p'))
        obj.reservation_time = str(
            localtime(start, tz).strftime('%m/%d/%Y %I:%M %p')) \
                               + ' - ' \
                               + str(localtime(end, tz)
                                     .strftime('%m/%d/%Y %I:%M %p')
                                     )
        return obj


class ReservationUpdateAPIView(UpdateAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationCreateUpdateSerializer
    permission_classes = [IsAuthenticated, isOwnerOrAdmin]


class ReservationDestroyAPIView(DestroyAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer
    permission_classes = [IsAuthenticated, isOwnerOrAdmin]


class ReservationListAPIView(ListAPIView):
    serializer_class = ReservationSerializer
    permission_classes = [IsAuthenticated]

    @method_decorator(cache_page(60))
    def dispatch(self, *args, **kwargs):
        return super(ReservationListAPIView, self).dispatch(*args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        if self.request.user.is_superuser:
            queryset_list = Reservation.objects.all()
        else:
            queryset_list = Reservation.objects.all().filter(
                user=self.request.user.id
            )
        reservations = list(queryset_list)
        for res in reservations:
           if self.request.session.get('django_timezone'):
               tzname = self.request.session.get('django_timezone')
           elif Profile.objects.filter(user=self.request.user).first().time_zone:
               tzname = Profile.objects.filter(user=self.request.user).first().time_zone
               self.request.session['django_timezone'] = tzname
           else:
               tzname='UTC'
           if tzname:
               timezone.activate(pytz.timezone(tzname))
           else:
               timezone.deactivate()

           dates = res.reservation_time.split(' - ')
           time_zone = res.time_zone
           if time_zone:
               tz = pytz.timezone(time_zone)
           else:
               tz = pytz.timezone('UTC')

           local_tz = pytz.timezone(tzname)
           start = local_tz.localize(datetime.strptime(dates[0], '%m/%d/%Y %I:%M %p'))
           end = local_tz.localize(datetime.strptime(dates[1], '%m/%d/%Y %I:%M %p'))
           res.reservation_time = str(
               localtime(start, tz).strftime('%m/%d/%Y %I:%M %p'))\
                                  + ' - '\
                                  + str(localtime(end, tz)
                                        .strftime('%m/%d/%Y %I:%M %p')
                                        )

        return reservations

