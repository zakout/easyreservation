from django import forms
from .models import Reservation

# Create the form class.


class ReservationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ReservationForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})


    class Meta:
        model = Reservation
        fields = ['title', 'resource', 'reservation_time']

