# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic.detail import DetailView

from django.views import View
from django.views.generic.edit import CreateView

from django.shortcuts import render
from django.shortcuts import get_object_or_404


from .forms import ReservationForm
from .models import Reservation
from accounts.models import Profile
from resources.models import Resource


class ReservationAddView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    template_name = "reservation_form.html"
    form_class = ReservationForm
    success_url = '/reservations/'

    def get_initial(self, *args, **kwargs):
        if self.request.GET.get('resource_id') and self.request.method=="GET":
            pk = self.request.GET.get('resource_id')
            resource = get_object_or_404(Resource, pk=pk)
            return {'resource': resource}

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        instance = form.save()
        instance.user = self.request.user
        if self.request.session.get('django_timezone'):
            instance.time_zone = self.request.session.get('django_timezone')
        else:
            instance.time_zone = Profile.objects.filter(
                user=self.request.user).first().time_zone

        instance.save()
        return super(ReservationAddView, self).form_valid(form)


class ReservationEditView(LoginRequiredMixin, DetailView):
    login_url = '/login/'
    queryset = Reservation.objects.all()
    template_name = "reservation_details.html"
    success_url = '/reservations/'


class ReservationListView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'reservations_list.html')


