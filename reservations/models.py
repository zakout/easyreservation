# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from django.db import models

from resources.models import Resource
from django.contrib.auth.models import User

# Create your models here.


class Reservation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    resource = models.ForeignKey(
        to=Resource,
        db_column='resource',
        related_name='reservations',
        verbose_name=_('resource'),
        db_constraint=False,
        on_delete=models.CASCADE,
        default=None
    )

    title = models.CharField(max_length=120, verbose_name=_('title'),)
    reservation_time = models.CharField(max_length=120,
                                        blank=True,
                                        verbose_name=_('Reservation Time'),
                                        )

    time_zone = models.CharField(max_length=120,
                                 blank=True,
                                 verbose_name=_('time_zone')
                                 )

    class Meta(object):
        managed = True
        db_table = 'reservations'
