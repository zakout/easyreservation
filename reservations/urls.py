from django.conf.urls import url

from .views import (
    ReservationAddView,
    ReservationListView,
    ReservationEditView,
)

urlpatterns = [
    url(r'^$', ReservationListView.as_view(), name='list'),
    url(r'^create/', ReservationAddView.as_view(), name='new'),
    url(r'(?P<pk>[\w-]+)/detail/', ReservationEditView.as_view(), name='detail'),

]