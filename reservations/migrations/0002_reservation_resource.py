# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-10-18 03:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('resources', '0008_auto_20181014_0641'),
        ('reservations', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='reservation',
            name='resource',
            field=models.ForeignKey(db_column='resource', db_constraint=False, default=None, on_delete=django.db.models.deletion.CASCADE, related_name='reservations', to='resources.Resource', verbose_name='reservations'),
        ),
    ]
