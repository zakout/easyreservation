# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-10-20 04:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reservations', '0004_auto_20181020_0414'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reservation',
            old_name='period',
            new_name='reservation_time',
        ),
    ]
