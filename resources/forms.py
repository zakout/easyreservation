from django import forms
from .models import Resource, ResourceType

# Create the form class.


class ResourceForm(forms.ModelForm):
    type = forms.ModelMultipleChoiceField(
        queryset=ResourceType.objects.all(),
        to_field_name="type"
        )

    def __init__(self, *args, **kwargs):
        super(ResourceForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    def clean(self):
        # Then call the clean() method of the super  class
        cleaned_data = super(ResourceForm, self).clean()
        # ... We assign the first entry in the qs to the type of the resource
        cleaned_data['type'] = cleaned_data['type'].first()
        # Finally, return the cleaned_data
        return cleaned_data


    class Meta:
        model = Resource
        fields = ['id', 'title', 'location', 'max_person', 'type']
