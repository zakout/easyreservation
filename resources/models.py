# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
from django.conf import settings

from django.utils.translation import ugettext_lazy as _


from django.db import models
from django.db.models.signals import pre_save

from utils.slug import unique_slug_generator


# Create your models here.


class Resource(models.Model):
    FREE = 1
    OCCUPIED = 2

    STATUS_CHOICES = (
        (FREE, 'free'),
        (OCCUPIED, 'occupied'),
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    title = models.CharField(max_length=120, verbose_name=_('title'))
    slug = models.SlugField(unique=True)
    type = models.ForeignKey(
        to='ResourceType',
        db_column='type',
        related_name='resources',
        verbose_name=_('resources'),
        db_constraint=False,
        on_delete=models.CASCADE,
    )

    location = models.CharField(max_length=120,
                                blank=True,
                                verbose_name=_('location')
                                )
    max_person = models.IntegerField(default=10, verbose_name=_('max person'))
    state = models.IntegerField(choices=STATUS_CHOICES, default=FREE)
    timestamp = models.DateTimeField(default=datetime.now)

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    class Meta(object):
        managed = True
        db_table = 'resources'


def upload_location(instance, filename):
    return instance.type+'.png'


class ResourceType(models.Model):
    type = models.CharField(max_length=120)
    image = models.ImageField(upload_to=upload_location,
                              null=True,
                              blank=True,
                              width_field="width_field",
                              height_field="height_field")
    height_field = models.IntegerField(default=100)
    width_field = models.IntegerField(default=100)

    def __unicode__(self):
        return self.type

    def __str__(self):
        return self.type

    class Meta(object):
        managed = True
        db_table = 'resrource_types'


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        # instance.slug = create_slug(instance)
        instance.slug = unique_slug_generator(instance)


pre_save.connect(pre_save_post_receiver, sender=Resource)
