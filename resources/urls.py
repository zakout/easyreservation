from django.conf.urls import url

from .views import (
    ResourceAddView,
    ResourceListView
)

urlpatterns = [
    url(r'^$', ResourceListView.as_view(), name='list'),
    url(r'^create/', ResourceAddView.as_view(), name='new'),
]