# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    PermissionRequiredMixin
)


from django.views import View
from django.views.generic.edit import CreateView

from django.shortcuts import render

from .forms import ResourceForm


class ResourceAddView(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    template_name = "resource_form.html"
    form_class = ResourceForm
    success_url = '/resources'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        instance = form.save()
        instance.user = self.request.user
        instance.save()
        return super(ResourceAddView, self).form_valid(form)


class ResourceListView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, 'resources_list.html')