from django.conf.urls import url

from .views import (
    ResourceCreateAPIView,
    ResourceListAPIView,
    ResourceDetailAPIView,
    ResourceDestroyAPIView,
    ResourceUpdateAPIView
)

urlpatterns = [
    url(
        r'^$',
        ResourceListAPIView.as_view(),
        name='list'
    ),
    url(
        r'^create/$',
        ResourceCreateAPIView.as_view(),
        name='create'
    ),
    url(
        r'^(?P<slug>[\w-]+)/$',
        ResourceDetailAPIView.as_view(),
        name='detail'
    ),
    url(
        r'^(?P<slug>[\w-]+)/edit/$',
        ResourceUpdateAPIView.as_view(),
        name='update'
    ),
    url(
        r'^(?P<pk>[\w-]+)/delete/$',
        ResourceDestroyAPIView  .as_view(),
        name='delete'
    ),
]
