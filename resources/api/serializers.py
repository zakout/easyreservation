# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
)

from resources.models import Resource


class ResourceSerializer(ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name="resources-api:detail",
        lookup_field="slug"
    )

    user = SerializerMethodField()
    image = SerializerMethodField()
    type = SerializerMethodField()

    class Meta:
        model = Resource
        fields = [
            'url',
            'title',
            'slug',
            'id',
            'type',
            'location',
            'max_person',
            'state',
            'user',
            'image',
        ]

    def get_user(self, obj):
        return str(obj.user.username)

    def get_image(self, obj):
        try:
            image = obj.type.image.url
        except:
            image = None
        return image

    def get_type(self, obj):
        return obj.type.type

class ResourceCreateUpdateSerializer(ModelSerializer):
    class Meta:
        model = Resource
        fields = [
            'title',
            'type',
            'location',
            'max_person',
        ]