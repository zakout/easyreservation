from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination
)


class ResourceLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 8
    max_limit = 10


class ResourcePageNumberPagination(PageNumberPagination):
    page_size = 33
