from django.db.models import Q

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    UpdateAPIView,
    RetrieveAPIView,
)

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly
)

from resources.models import Resource
from .pagination import (
    ResourcePageNumberPagination
)
from .serializers import (
    ResourceSerializer,
    ResourceCreateUpdateSerializer,
)


class ResourceCreateAPIView(CreateAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceCreateUpdateSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ResourceDetailAPIView(RetrieveAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'slug'


class ResourceUpdateAPIView(UpdateAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceCreateUpdateSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]
    lookup_field = 'slug'


class ResourceDestroyAPIView(DestroyAPIView):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    permission_classes = [IsAuthenticated, IsAdminUser]


class ResourceListAPIView(ListAPIView):
    serializer_class = ResourceSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = ResourcePageNumberPagination

    @method_decorator(cache_page(60))
    def dispatch(self, *args, **kwargs):
        return super(ResourceListAPIView, self).dispatch(*args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        queryset_list = Resource.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(type__type__icontains=query) |
                Q(location__icontains=query)
            ).distinct()
        return queryset_list

