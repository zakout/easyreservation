# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import pytz

from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import render
from django.views import View

# Create your views here.


class ProfileSettingsView(LoginRequiredMixin, View):
    def get(self, request):
        return render(
            request,
            'settings.html',
            {'time_zones': [(x, x) for x in pytz.common_timezones],
             'time_zone': self.request.session.get('django_timezone'),
             'language_code': self.request.session.get('language_code')
             }
        )
