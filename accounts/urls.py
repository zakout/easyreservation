from django.conf.urls import url

from .views import ProfileSettingsView

urlpatterns = [
    url(r'^settings/$', ProfileSettingsView.as_view(), name='settings'),

]