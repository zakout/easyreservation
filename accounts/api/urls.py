from django.conf.urls import url

from .views import (
    UserLoginAPIView,
    switch_settings
    )

urlpatterns = [
    url(r'^login/$', UserLoginAPIView.as_view(), name='login'),
    url(r'^switch_settings/$', switch_settings, name='switch_language'),
]