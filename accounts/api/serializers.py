from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.db.models import Q

from rest_framework.serializers import (
    CharField,
    EmailField,
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError
)

User = get_user_model()


class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'is_superuser',
        ]


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class UserLoginSerializer(ModelSerializer):
    username = CharField(required=False, allow_blank=True)
    email = EmailField(label='Email Address', required=False, allow_blank=True)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'is_superuser'

        ]
        extra_kwargs = {"password":
                            {"write_only": True}
                        }

    def validate(self, data):
        user_obj = None
        email = data.get('email')
        username = data.get('username')
        password = data.get('password')
        if not email and not username:
            raise ValidationError("A username or email is required to login.")

        if email:
            user = User.objects.filter(email=email).distinct()
        if username:
            user = User.objects.filter(username=username).distinct()

        user.exclude(email__isnull=True).exclude(email__iexact='')
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError("Username/Email is not valid")
        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError('Incorrect password.')
        result = {"username": username,
                  "email": email,
                  "is_superuser": user_obj.is_superuser
                  }
        return result

