from django.contrib.auth import get_user_model

from django.http import HttpResponse

from django.utils import translation

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView


from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,

)

User = get_user_model()

from accounts.models import Profile

from .serializers import (
    UserLoginSerializer,
)


class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


def switch_settings(request):

    language_code = request.GET.get('code')
    time_zone = request.GET.get('time_zone')
    profile = Profile.objects.filter(user=request.user).first()
    if language_code:
        profile.language_code = language_code
        profile.save()
        user_language = language_code
        request.session['language_code'] = language_code

        translation.activate(user_language)
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
    if time_zone:
        profile.time_zone = time_zone
        request.session['django_timezone'] = time_zone
    return HttpResponse('Settings changed', status=HTTP_200_OK)













